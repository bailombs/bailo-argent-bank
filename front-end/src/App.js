import React from 'react';
import { Route, Routes } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import Home from './pages/Home';
import Login from './pages/Login';
import Profile from './pages/Profile';
import Transactions from './pages/Transactions';
import './styles/index.scss';

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='/sign-in' element={<Login />} />

      {/* private routes */}

      <Route element={<PrivateRoute />}>
        <Route path='/profile' element={<Profile />} ></Route>
        <Route path='/profile/transaction' element={<Transactions />} />
      </Route>
    </Routes>
  );
};

export default App;