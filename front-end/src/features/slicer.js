import { createSlice } from "@reduxjs/toolkit";

export const slice = createSlice({
    name: 'authentication',
    initialState: {
        user: '',
        token: '',
        firstName: '',
        lastName: ''
    },
    reducers: {
        postData: (state, action) => {
            state.user = action.payload
        },
        getToken: (state, action) => {
            state.token = action.payload;
        },

        logOut: (state) => {
            state.token = null
            state.user = null
        },

        firstName: (state, action) => {
            state.firstName = action.payload
        },
        lastName: (state, action) => {
            state.lastName = action.payload
        }

    }

})
export default slice.reducer;
export const { postData, getToken, logOut, firstName, lastName } = slice.actions