import { configureStore } from "@reduxjs/toolkit";
import connectReducer from '../features/slicer.js';

export default configureStore({
    reducer: {
        connect: connectReducer,
    }
})