import React, { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Header from '../components/Header';

const Transactions = () => {

    const naviagte = useNavigate()
    const location = useLocation()

    useEffect(() => {
        if (location.pathname === '/profile/transaction') {
            setTimeout(
                () => {
                    naviagte('/profile')
                },
                1000
            )
        }
    }, [naviagte, location])



    return (
        <React.Fragment>
            <Header page='profile' />
            <h1 className='transaction'>
                Les Données de la page ne sont pas encore disponibles. Elles le seront bientôt
            </h1>
        </React.Fragment>
    );
};

export default Transactions;