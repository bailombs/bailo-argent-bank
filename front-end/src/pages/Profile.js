import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Navigate, useLocation, useNavigate } from 'react-router-dom';
import { apiProfile, PROFILE_ROUTE } from '../api/routes';
import ContainerUserContent from '../components/ContainerUserContent';
import Footer from '../components/Footer';
import Header from '../components/Header';
import { firstName, lastName } from '../features/slicer';

const Profile = () => {

    const dispach = useDispatch()
    const token = sessionStorage.getItem('token')
    // const token = sessionStorage.getItem('token');
    const naviagte = useNavigate()
    const location = useLocation()


    useEffect(() => {

        if (token) {
            <Navigate to='/profile' state={{ from: location }} replace />
        }
    }, [naviagte, token, location])

    /**
     *  function getProfil from the api
     * @param {string} token 
     */
    const getProfile = async (token) => {

        try {

            const res = await apiProfile(PROFILE_ROUTE, token)
            dispach(firstName((res.data?.body?.firstName)));
            dispach(lastName((res.data?.body?.lastName)));

        } catch (error) {
            console.log(error);
        }

    }

    getProfile(token);

    return (
        <React.Fragment>
            <Header page='profile' />
            <ContainerUserContent />
            <Footer />
        </React.Fragment>

    );
};

export default Profile;